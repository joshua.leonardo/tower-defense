// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Spawner.generated.h"

UCLASS()
class TOWER_DEFENSE_API ASpawner : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASpawner();

	UFUNCTION()
		AEnemy* SpawnEnemy(TSubclassOf<class AEnemy> Enemies, int32 waveCount);

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		float difficultyScale;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		class USceneComponent* sceneComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray <class AWay_Point*> Waypoints;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;


};
