// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "MyPlayer.generated.h"

UCLASS()
class TOWER_DEFENSE_API AMyPlayer : public APawn
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AMyPlayer();

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		class UHealthComponent* HealthComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		int32 playerGold;

	UFUNCTION(BlueprintPure, BlueprintCallable)
		bool canBuy(int32 price);

	UFUNCTION(BlueprintCallable)
		void onSell(int32 price);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
