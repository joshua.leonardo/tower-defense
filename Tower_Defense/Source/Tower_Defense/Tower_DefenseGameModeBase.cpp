// Copyright Epic Games, Inc. All Rights Reserved.


#include "Tower_DefenseGameModeBase.h"
#include "Spawner.h"
#include "WaveData.h"
#include "Enemy.h"
#include "MyPlayer.h"
#include "HealthComponent.h"


void ATower_DefenseGameModeBase::BeginPlay()
{
	Super::BeginPlay();

	player = Cast<AMyPlayer>(GetWorld()->GetFirstPlayerController()->GetPawn());
}

void ATower_DefenseGameModeBase::SpawnEnemy()
{
	FTimerHandle timerHandle;

	maxSpawnableEnemies = WaveCount[currentWave]->maxSpawns;

	if (currentSpawnedEnemies < maxSpawnableEnemies)
	{
		GetWorldTimerManager().SetTimer(timerHandle, this, &ATower_DefenseGameModeBase::SpawnEnemy, 2.5, false);
		AEnemy* Enemy = Spawner[FMath::RandRange(0, 3)]->SpawnEnemy(WaveCount[currentWave]->Enemies[0], currentWave);
		Enemy->healthComponent->OnDeath.AddDynamic(this, &ATower_DefenseGameModeBase::reward);
		currentSpawnedEnemies++;
		WaveCount[currentWave]->numberOfSpawns = currentSpawnedEnemies;
	}

	else if (currentSpawnedEnemies >= maxSpawnableEnemies)
	{
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("Wave Cleared"));
		if (playerKillCount >= currentSpawnedEnemies)
		{
			waveCleared();
		}
	}
}

void ATower_DefenseGameModeBase::reward(UHealthComponent* HealthComponent)
{
	if (currentWave <= WaveCount.Num())
	{
		playerKillCount++;
		player->playerGold += WaveCount[currentWave]->killReward;

	}
}

void ATower_DefenseGameModeBase::waveCleared()
{
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("Wave 2"));
	player->playerGold += WaveCount[currentWave]->winReward;

	currentWave++;
	playerKillCount = 0;
	player->HealthComponent->currentHealth = player->HealthComponent->maxHealth;
	WaveCount[currentWave]->winReward *= 1.1f;
	WaveCount[currentWave]->killReward *= 1.1f;
	currentSpawnedEnemies = 0;

	SpawnEnemy();
}
