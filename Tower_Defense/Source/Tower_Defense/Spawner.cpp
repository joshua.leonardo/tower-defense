// Fill out your copyright notice in the Description page of Project Settings.


#include "Spawner.h"
#include "Enemy.h"
#include "Way_Point.h"
#include "HealthComponent.h"
#include "Components/SceneComponent.h"

// Sets default values
ASpawner::ASpawner()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	sceneComponent = CreateDefaultSubobject<USceneComponent>("SceneComponent");
	SetRootComponent(sceneComponent);
}

AEnemy* ASpawner::SpawnEnemy(TSubclassOf<class AEnemy> Enemies, int32 waveCount)
{
	FActorSpawnParameters SpawnParameters;
	SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;
	
	difficultyScale = 1 + (waveCount * .1);
	
	AEnemy* spawnedEnemies = GetWorld()->SpawnActor<AEnemy>(Enemies, GetActorLocation(), FRotator(30), SpawnParameters);
	spawnedEnemies->healthComponent->ScaleHealth(difficultyScale);
	spawnedEnemies->setWaypoints(Waypoints);

	return spawnedEnemies;
}

// Called when the game starts or when spawned
void ASpawner::BeginPlay()
{
	Super::BeginPlay();
}


// Called every frame
void ASpawner::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

