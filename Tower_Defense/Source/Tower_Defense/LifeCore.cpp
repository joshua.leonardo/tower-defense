// Fill out your copyright notice in the Description page of Project Settings.


#include "LifeCore.h"
#include "Enemy.h"
#include "MyPlayer.h"
#include "HealthComponent.h"
#include "Components/SceneComponent.h"
#include "Components/StaticMeshComponent.h"

// Sets default values
ALifeCore::ALifeCore()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	sceneComponent = CreateDefaultSubobject<USceneComponent>("SceneComponent");
	SetRootComponent(sceneComponent);
	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>("StaticMesh");
	StaticMesh->SetupAttachment(sceneComponent);

	StaticMesh->OnComponentBeginOverlap.AddDynamic(this, &ALifeCore::OnOverlap);
}

// Called when the game starts or when spawned
void ALifeCore::BeginPlay()
{
	Super::BeginPlay();

	myPlayer = Cast<AMyPlayer>(GetWorld()->GetFirstPlayerController()->GetPawn()); 
}

void ALifeCore::OnOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("Overlapped"));
	if (AEnemy* enemy = Cast<AEnemy>(OtherActor))
	{
		myPlayer->HealthComponent->TakeDamage(1.0f);
		enemy->Destroy();
		if (myPlayer->HealthComponent->currentHealth <= 0)
		{
			GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("Player Died"));
		}
	}
}

// Called every frame
void ALifeCore::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

