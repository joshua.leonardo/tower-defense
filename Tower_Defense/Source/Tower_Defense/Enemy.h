// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "Enemy.generated.h"



UCLASS()
class TOWER_DEFENSE_API AEnemy : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	AEnemy();
	
	UPROPERTY(BlueprintReadOnly, EditAnywhere)
		float damage;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		bool isGroundUnit;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		bool isNormalEnemy;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		bool isAlive;

	UPROPERTY(VisibleAnywhere)
		class UHealthComponent* healthComponent;

	UFUNCTION()
		void setWaypoints(TArray<class AWay_Point*> waypoints);


	int32 wayPointCounter;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere)
		class UStaticMeshComponent* StaticMesh;

	UPROPERTY(VisibleAnywhere)
		class UFloatingPawnMovement* PawnMovement;

	UPROPERTY(VisibleAnywhere)
		class USceneComponent* sceneComponent;

	UFUNCTION()
		void MoveToWaypoints();

	UFUNCTION(BlueprintCallable)
		void Die(UHealthComponent* Health);


public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;	

private:
	UPROPERTY(VisibleAnywhere)
		TArray<class AWay_Point*> Waypoints;
};
