// Fill out your copyright notice in the Description page of Project Settings.


#include "Enemy.h"
#include "EnemyController.h"
#include "Way_Point.h"
#include "HealthComponent.h"
#include "GameFramework/FloatingPawnMovement.h"	
#include "Components/SceneComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Kismet/KismetMathLibrary.h"

// Sets default values
AEnemy::AEnemy()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	sceneComponent = CreateDefaultSubobject<USceneComponent>("SceneComponent");
	SetRootComponent(sceneComponent);
	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>("StaticMesh");
	StaticMesh->SetupAttachment(sceneComponent);

	PawnMovement = CreateDefaultSubobject<UFloatingPawnMovement>("PawnMovement");

	healthComponent = CreateDefaultSubobject<UHealthComponent>("HealthComponent");
}

void AEnemy::setWaypoints(TArray<class AWay_Point*> waypoints)
{
	Waypoints = waypoints;
}

// Called when the game starts or when spawned
void AEnemy::BeginPlay()
{
	Super::BeginPlay();
	SpawnDefaultController();

	healthComponent->OnDeath.AddDynamic(this, &AEnemy::Die);
}

void AEnemy::MoveToWaypoints()
{
	FRotator Rotation = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), Waypoints[wayPointCounter]->GetActorLocation());
	SetActorRotation(Rotation);
	PawnMovement->AddInputVector(GetActorForwardVector());
}

void AEnemy::Die(UHealthComponent* Health)
{
	isAlive = false;
	Destroy();
}

// Called every frame
void AEnemy::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (Waypoints.Num() <= wayPointCounter) return;

	MoveToWaypoints();

	if ((GetActorLocation() - Waypoints[wayPointCounter]->GetActorLocation()).Size() <= 50)
	{
		wayPointCounter++;
	}
}

// Called to bind functionality to input
void AEnemy::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

