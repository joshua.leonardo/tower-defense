// Fill out your copyright notice in the Description page of Project Settings.


#include "Way_Point.h"
#include "Components/StaticMeshComponent.h"

// Sets default values
AWay_Point::AWay_Point()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>("StaticMesh");
	SetRootComponent(StaticMesh);

}

// Called when the game starts or when spawned
void AWay_Point::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AWay_Point::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

