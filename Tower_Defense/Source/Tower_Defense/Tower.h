// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Tower.generated.h"

UCLASS()
class TOWER_DEFENSE_API ATower : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATower();
	
	bool canFire = true;

	bool isLevel1 = true;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		bool isScavenger;

	UFUNCTION()
		void setEnemies(TArray<class AEnemy*> Enemies);

	UFUNCTION()
		void removeEnemy(class UHealthComponent* HealthComponent);

	UFUNCTION(BlueprintCallable)
		void gainGold();

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		TArray<class AEnemy*> enemies;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		TSubclassOf<class AProjectile> projectile;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		class AMyPlayer* player;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere)
		class USceneComponent* sceneComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		class UStaticMeshComponent* StaticMesh;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class USphereComponent* sphereCollider;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class UArrowComponent* arrowComponent;

	UFUNCTION()
		void OnOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
		void OnEndOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	UFUNCTION()
		void gainExtraGoldOnKill(UHealthComponent* HealthComponent);
	
	UFUNCTION()
		void Target();

	UFUNCTION()
		void spawnProjectile();

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
