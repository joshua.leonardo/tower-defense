// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "HealthComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnDeath, UHealthComponent*, Health);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TOWER_DEFENSE_API UHealthComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UHealthComponent();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		FOnDeath OnDeath;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		float currentHealth;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		float maxHealth;

	UFUNCTION(BlueprintCallable)
		void TakeDamage(int damage);

	UFUNCTION()
		void ScaleHealth(float multiplier);

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

		
};
