// Fill out your copyright notice in the Description page of Project Settings.


#include "Tower.h"
#include "Enemy.h"
#include "HealthComponent.h"
#include "Projectile.h"
#include "MyPlayer.h"
#include "Components/SceneComponent.h"
#include "Components/SphereComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Components/ArrowComponent.h"
#include "Kismet/KismetMathLibrary.h"

// Sets default values
ATower::ATower()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	sceneComponent = CreateDefaultSubobject<USceneComponent>("SceneComponent");
	SetRootComponent(sceneComponent);
	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>("StaticMesh");
	StaticMesh->SetupAttachment(sceneComponent);

	sphereCollider = CreateDefaultSubobject<USphereComponent>("Sphere Collider");
	sphereCollider->SetupAttachment(sceneComponent);
	sphereCollider->OnComponentBeginOverlap.AddDynamic(this, &ATower::OnOverlap);
	sphereCollider->OnComponentEndOverlap.AddDynamic(this, &ATower::OnEndOverlap);

	arrowComponent = CreateDefaultSubobject<UArrowComponent>("Arrow Component");
	arrowComponent->SetupAttachment(StaticMesh);
}

void ATower::setEnemies(TArray<class AEnemy*> Enemies)
{
	enemies = Enemies;
}

void ATower::removeEnemy(UHealthComponent* HealthComponent)
{
	if (AEnemy* enemy = Cast<AEnemy>(HealthComponent->GetOwner()))
	{
		enemies.Remove(enemy);
	}
}

void ATower::gainGold()
{
	player->playerGold += 1;
}

// Called when the game starts or when spawned
void ATower::BeginPlay()
{
	Super::BeginPlay();
	
	player = Cast<AMyPlayer>(GetWorld()->GetFirstPlayerController()->GetPawn());
}

void ATower::OnOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (AEnemy* enemy = Cast<AEnemy>(OtherActor))
	{
		enemies.Add(enemy);
		enemy->healthComponent->OnDeath.AddDynamic(this, &ATower::removeEnemy);
		enemy->healthComponent->OnDeath.AddDynamic(this, &ATower::gainExtraGoldOnKill);
	}
}

void ATower::OnEndOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (AEnemy* enemy = Cast<AEnemy>(OtherActor))
	{
		enemies.Remove(enemy);
		enemy->healthComponent->OnDeath.RemoveDynamic(this, &ATower::removeEnemy);
		enemy->healthComponent->OnDeath.AddDynamic(this, &ATower::gainExtraGoldOnKill);
	}
}

void ATower::gainExtraGoldOnKill(UHealthComponent* HealthComponent)
{
	if (isLevel1)
	{
		player->playerGold += 30;
	}
	
	else if (!isLevel1)
	{
		player->playerGold += 60;
	}
}

void ATower::Target()
{
	if (!isScavenger) {
		FRotator rotation = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), enemies[0]->GetActorLocation());
		StaticMesh->SetWorldRotation(FRotator(0, rotation.Yaw - 90, 0));
	}
}

void ATower::spawnProjectile()
{
	FTimerHandle timerHandle;
	FTimerDelegate timerDelegate;
	FActorSpawnParameters spawnParameters;

	spawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;
	canFire = false;

	AProjectile* spawnedProjectile = GetWorld()->SpawnActor<AProjectile>(projectile, arrowComponent->GetComponentTransform(), spawnParameters);
	timerDelegate.BindLambda([this] { canFire = true;  });

	GetWorld()->GetTimerManager().SetTimer(timerHandle, timerDelegate, 1.0f, false);
}

// Called every frame
void ATower::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (enemies.Num() <= 0) return;

	if (enemies[0] != nullptr)
	{
		Target();
	}

	if (canFire && !isScavenger)
	{
		spawnProjectile();
	}
}

