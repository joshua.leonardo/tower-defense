// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Tower_DefenseGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class TOWER_DEFENSE_API ATower_DefenseGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
public:

	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		int32 currentWave = 0;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		int32 playerKillCount = 0;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray<class ASpawner*> Spawner;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		int32 currentSpawnedEnemies;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		int32 maxSpawnableEnemies;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		TArray<class UWaveData*> WaveCount;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		class AMyPlayer* player;

	UFUNCTION(BlueprintCallable)
		void SpawnEnemy();

	UFUNCTION()
		void reward(UHealthComponent* HealthComponent);

	UFUNCTION()
		void waveCleared();
};
