// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "WaveData.generated.h"

/**
 * 
 */
UCLASS()
class TOWER_DEFENSE_API UWaveData : public UDataAsset
{
	GENERATED_BODY()

public: 
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		float timeDuration;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		int32 numberOfSpawns;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		int32 maxSpawns;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		TArray<TSubclassOf<class AEnemy>> Enemies;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		int32 killReward;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		int32 winReward;
};
