// Fill out your copyright notice in the Description page of Project Settings.


#include "MyPlayer.h"
#include "HealthComponent.h"
#include "LifeCore.h"

// Sets default values
AMyPlayer::AMyPlayer()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	HealthComponent = CreateDefaultSubobject<UHealthComponent>("HealthComponent");
}

bool AMyPlayer::canBuy(int32 price)
{
	if (playerGold >= price)
	{
		playerGold -= price;
		return true;
	} 
	else
	{
		return false;
	}
}

void AMyPlayer::onSell(int32 price)
{
	price /= 2;
	playerGold += price;
}

// Called when the game starts or when spawned
void AMyPlayer::BeginPlay()
{
	Super::BeginPlay();

	playerGold = 200;
}

// Called every frame
void AMyPlayer::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

